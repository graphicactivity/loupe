// Git init
$(document).ready(function(){
    // Menu button
	$('.menu').click(function(){
		$(this).toggleClass('open');
	});
	$('.menuTitle').mouseover(function(){
		$('.menu').toggleClass('open');
	});
    $('.menu').mouseout(function(){
        $(this).removeClass('open');
    });
    $('.menuItems').mouseover(function(){
        $('.menu').addClass('open');
    });
    $('.menuShape').mouseout(function(){
        $('.menu').removeClass('open');
    });
	// Video video lightbox
	$('.video').click(function(){
        $(this).addClass('showLB');
    });
	$('.closeLightbox').click(function(){
		$('.video').removeClass('showLB');
	});
	// And with esc
    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // esc keycode
            $('.video').removeClass('showLB');
        }
    });
});
